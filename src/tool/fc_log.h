/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef LISTS_LOG_H
#define LISTS_LOG_H

#include <fc_config.h>

#define INFO_LOG_NAME "fc_info.log"
#define ERRO_LOG_NAME "fc_error.log"
#define NOTI_LOG_NAME "fc_notice.log"
#define WARN_LOG_NAME "fc_warning.log"

/**
 * @brief Notice
 * Not to invoke the function directly, instead using the logger(...) function
 * for the reason LOG_LEVEL control.
 */
void _logger(int type, char *str, ...);

#if FC_DEBUG
    #define logger(type, str, ...) _logger(type, str, ##__VA_ARGS__)
#else
    #define logger(type, str, ...) do{\
        if (type > LOG_WARNING) {\
            _logger(type, str, ##__VA_ARGS__);\
        }} while(0);
#endif

#define ERROR_CODE_FMT "code: %d, msg: %s"
#define ERROR_CODE_STR  errno, strerror(errno)

#endif /* LISTS_LOG_H */
